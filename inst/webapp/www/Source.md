The code is available under the terms of the [Affero GPL license v3](https://www.gnu.org/licenses/agpl-3.0.en.html) at: the [GitLab repository](https://git-r3lab.uni.lu/andras.hartmann/seesaw)
