## Documentation
---
### File formats
- The required input of SeesawPred is a tab separated value file where columns represent
gene expression (microarray or RNA-Seq) replicas of the progenitor cell-type and the two daughter cell-types 
labeled as "Progenitor", "Daughter1", "Daughter2", respectively, and the rows are labeled according to the gene symbols. See an example dataset
<a href="manual/dataset_example.csv" target="_blank">here</a>.
- The Prior Knowledge Network (PKN) is a network consisting of TF–TF regulatory interactions, that serve as potential links in the network.
The file format PKN is also a tab separated values file containing the list of TF - TF interactions, where the first and the third column corresponds to source and target TF, and second colum describes the interaction type which can be "Activation", "Inhibition" or "Unspecified".
See an example PKN <a href="manual/PKN_example.csv" target="_blank">here</a>.

### Application walkthrough
 <table style="width:100%">
  <tr> <td> <h4> 
     Upload transcriptomic files
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/CSV_upload.png" alt="Transcriptomic data chooser" title="File chooser for transcriptomic data"> 
  </td> <td>
      Custom input files containing transcriptomics data can be uploaded via the file chooser. The upload expects a tab separated value file containing the progenitor and daughter cell-type data. Valid extensions are .csv, .tsv and .txt . Naming convention for columns is "Parental.", "Daugher1.", and "Daughter2." for progenitor and the two daugther cell-types. Gene names should be in gene symbol format (HGNC for human and MGI for mouse). 
  </td> </tr>
  
  <tr> <td> <h4> 
     Upload prior knowledge network
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/PKN.png" alt="Chooser for prior knowledge network PKN" title="Prior knowledge network chooser"> 
  </td>  <td>
      Prior knowledge networks can be uploaded via the PKN chooser. 
     The upload expects a tab separated value file containing a directed edge list: The first column contains is the regulator gene, the second column contains sign information (Activation, Inhibition or Unspecified), and the third column is the regulated gene, (e.g.: MYBL1 Inhibition  SP1) . Valid extensions are .csv, .tsv and .txt . If you do not have access to PKN, you can choose from two limited-size prior knowledge networks, currently available for mouse and human.
  </td> </tr>
  
  <tr> <td> <h4> 
     Set the parameters
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/options.png" alt="Paramterization" title="Parameters"> 
  </td>  <td>
      The application has two tuning parameters. Complexity controls the number of Gene Regulatory Networks (GRNs) that are subject to SCC detection: Basic => 10, Complex => 100, Advanced => 1000 networks. Strategy controls the P-value for the significance test.
  </td> </tr>
  
  <tr> <td> <h4> 
    Try an example
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/example.png" alt="Differentiation examples" title="Examples"> 
  </td>  <td>
      The application contains various binary cell differentiation examples that can be loaded using the example chooser.
      The example chooser also sets the correct PKN and the tuning parameters.
  </td> </tr>
  
  <tr> <td> <h4> 
      Run the analysis
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/run.png" alt="Run the analysis" title="Run!"> 
  </td>  <td>
      If you have followed the above steps, (e.g. you have uploaded your data, and set the PKN or loaded an example), this button turns active, and you can run the analysis by clicking on the button. Depending on the options you have set the analysis might take some time.
  </td> </tr>
  
  <tr> <td> <h4> 
      Results
  </h4> </td> </tr>
  <tr> <td>
      <img src="manual/results.png" alt="Result browser" title="Results"> 
  </td>  <td>
      The result browser is to visualize the results the output of the application. Detailed results are shown when you click on a line in the result browser.
  </td> </tr>
</table> 

---



