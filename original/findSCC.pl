#!/usr/bin/perl -w

#Use the location of the library
use lib '/Users/andras.hartmann/perl5/lib/perl5';
use strict;
use Graph::Directed;

sub scc;

my $file;
while ( @ARGV ) {
    my $arg = shift(@ARGV);
    if ( $arg eq "-f" ) {
        $file = shift(@ARGV);
    }
    else {
        die "Usage:./parseGeneFile.pl -f file.txt  : $!";
    }
}

my $pre;
if($file=~/^(.+)\.dat/){
    $pre=$1;
}
my $out= $file.".scc";

my @net;
open(FILE, $file) or die "could not open $file: $!";
while (my $line = <FILE>) { 
    next if $line =~ /^\s+$/;
    chomp $line;
    push @net, $line;
}
close FILE;

my $a_ref_scc = scc(@net);

open(OUT, ">$out") or die "Cannot open '$out': $!";
print OUT "Gene1 Arrow Gene2\n";
for my $l (@{$a_ref_scc}){
    for(@{$l}){
	print OUT join(" ",$_),"\n";
    }
}
close OUT;


sub scc{
    my (@network) = @_;
    my %self_loops;
    my @SCC=();
    my $network =Graph::Directed->new;
    foreach my $interaction(@network){
        $interaction =~ /(.+)(\s-[\>\|]\s)(.+)/;
        $network ->add_edge($1,$3);
        if ($1 eq $3){
            $self_loops{$&}=$&;
        }
        else{}
    }
    my @scc_raw = $network->strongly_connected_components;
    #Removing isolated nodes
    my @scc_curated =();
    foreach my $scc(@scc_raw){
        if ((scalar @{$scc})>1){
            push @scc_curated, $scc;
        }
        else{}
    }
    #Getting interactions
    foreach my $scc_curated(@scc_curated){
        my @nodes = @{$scc_curated};
        my %nodes = ();
        my @scc_interactions=();
        foreach my $node(@nodes){
            $nodes{$node}=$node;
        }
        foreach my $interaction(@network){
            $interaction =~ /(.+)(\s-[\>\|]\s)(.+)/;
            if ((exists $nodes{$1}) and (exists $nodes{$3})){
                push @scc_interactions, $&;
            }
            else{}
            if (($1 eq $3) and (exists $nodes{$1})){
                delete $self_loops{$&};
            }
            else{}
        }
        push @SCC, \@scc_interactions;
    }
    #Adding self-loops
    foreach my $self_loop(keys %self_loops){
        my @self_loop = ($self_loop);
        push @SCC, \@self_loop;
    }
    return (\@SCC);
   
}
