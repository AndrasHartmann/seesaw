#!/usr/bin/env Rscript

## Usage: computeNRD.R data_matrix_onlyTFs.csv list_of_DETFs.txt

## Import all.txt files in the current directory
args = commandArgs(TRUE)
file1 = args[1]
file1 = 'data_sample_onlyTFs.csv'
#file1= 'testdata_sample.csv'

outfile = paste0("significantNRD_",file1)

## Read in files
data = read.csv(file1,sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# Delete "." from colnames
colnames(data) = gsub("^(.+?)\\.\\d+$","\\1",colnames(data))

exp.tf = as.data.frame(data)
exp.tf = cbind('Symbol'=rownames(exp.tf), exp.tf)

## Ratios
#ASK: does this really compute the same as described???
#input values: are they log2 values?
ratio.outer = list()
for(i in 2:ncol(exp.tf)){
    m = outer(exp.tf[,i], exp.tf[,i], "-")
    ratio.outer[[i]] = m
}

## Take random combinations and compute ratio difference
computeNRD <- function(ratio.outer, com, daughter='Daughter1'){
    M = matrix(nrow = (nrow(ratio.outer[[2]])^2 /2 - nrow(ratio.outer[[2]])/2), ncol = n.replicates)
    for(i in 1:ncol(com)){
        comb = com[,i]
        ## Compute the ratio difference
        ratio.diff = ratio.outer[[comb[1]]] - ratio.outer[[comb[2]]]
        # Option 1: Divide the ratio difference by the ESC ratio
        ratio.diff = ratio.diff / ratio.outer[[comb[1]]]
        M[,i] = ratio.diff[upper.tri(ratio.diff)]
    }
    I = which(upper.tri(ratio.outer[[2]]),arr.ind=TRUE)
    genes = sprintf("%s__%s", exp.tf[,'Symbol'][I[,1]], exp.tf[,'Symbol'][I[,2]]); rownames(M) = genes
    colnames(M) = rep(paste0('Parental_',daughter),n.replicates)

    ## Convert infinite values into real number
    M[M==Inf] = 10000
    M[M==-Inf] = -10000

    ##ASK: Why the hack are we doing this binning?
    # Especially why with the first instance?
    ## Intensity binning
    intensity = cbind(
        exp.tf[match(exp.tf[I[,1],'Symbol'], exp.tf[,1]),'Parental'],
        exp.tf[match(exp.tf[I[,2],'Symbol'], exp.tf[,1]),'Parental'],
        exp.tf[match(exp.tf[I[,1], 'Symbol'], exp.tf[,1]),daughter],
        exp.tf[match(exp.tf[I[,2], 'Symbol'], exp.tf[,1]),daughter]
        )
    intensity = rowSums(intensity,na.rm=TRUE)
    names(intensity) = genes

    library(geneplotter)
    #multidensity(M,xlim=c(-10,10))
    ## MAD normalization
    for(i in 1:ncol(M)){
        M[,i] = (M[,i] - median(M[,i],na.rm=TRUE)) / mad(M[,i],na.rm=TRUE)
    }
    #multidensity(M,xlim=c(-10,10))

    ## Simple limma test
    K = 30
    #ASK: simple t-test only works for log values, do we have those?
    library(limma)
    q = quantile(intensity, probs=seq(0,1,length.out=K+1))
    I = cut(intensity, breaks=q,labels=FALSE)
    I[is.na(I)] = 1
    pval = rep(NA_real_,nrow(M))
    padj = rep(NA_real_,nrow(M))
    for (k in 1:K) {
      #Calculate p-values
        fit = eBayes(lmFit(M[I == k,]))
        pval[I == k] = fit$p.value
        #Adjust with Benjamini Hochberg
        padj[I == k] = p.adjust(fit$p.value,method="BH")
        print(sum(padj[I == k] <= 0.05,na.rm=TRUE))
    }
    names(padj) = rownames(M)
    hist(padj); length(padj)
    length(which(padj <= 0.05))

    ratio = cbind(padj,'mean.ratio.difference'=rowMeans(M))

    return(ratio)
}
# Randomized trials: which parent is associated with which daughter
# 1. Parental - Daughter1
# TODO: removed randomized
# ASK: should not rather be all possibilities?
n.replicates = 3
com = rbind(
    #sample(which(colnames(exp.tf)=='Parental'), n.replicates,replace=FALSE),
    #sample(which(colnames(exp.tf)=='Daughter1'), n.replicates,replace=FALSE)
    which(colnames(exp.tf)=='Parental'),
    which(colnames(exp.tf)=='Daughter1')
    )
ratio1 = computeNRD(ratio.outer, com, daughter='Daughter1')

# 2. Parental - Daughter2
n.replicates = 3
com = rbind(
    #sample(which(colnames(exp.tf)=='Parental'), n.replicates,replace=FALSE),
    #sample(which(colnames(exp.tf)=='Daughter2'), n.replicates,replace=FALSE)

    which(colnames(exp.tf)=='Parental'),
    which(colnames(exp.tf)=='Daughter2')
    )
ratio2 = computeNRD(ratio.outer, com, daughter='Daughter2')


## Find significant NRD pairs
sig1 = ratio1[ratio1[,'padj']<=0.05 & abs(ratio1[,'mean.ratio.difference'])>=0.5, ]
sig2 = ratio2[ratio2[,'padj']<=0.05 & abs(ratio2[,'mean.ratio.difference'])>=0.5, ]
g1 = rownames(sig1)
g2 = rownames(sig2)

## Export all intersection ratio pairs
g12 = intersect(g1,g2)
G12 = g12[sign(sig1[match(g12, rownames(sig1)),2] * sig2[match(g12, rownames(sig2)),2]) == -1]
T = cbind(sig1[rownames(sig1) %in% G12,], sig2[rownames(sig2) %in% G12,])

## Add mean gene expression
m = do.call('cbind',lapply(unique(colnames(data)),function(x){
    rowMeans(data[,colnames(data) %in% x])
}))
colnames(m) = unique(colnames(data))
temp = do.call('rbind',strsplit(rownames(T),"__"))
a = cbind(m[match(temp[,1], rownames(m)),], m[match(temp[,2], rownames(m)),])
colnames(a) = paste0(colnames(a),c('_left','_left','_left','_right','_right','_right'))
T = cbind(T,a)
write.table(T, outfile,sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)





