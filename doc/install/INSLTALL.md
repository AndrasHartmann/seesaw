# Seesaw Installation Guide

## Config and script files

- restartshiny.sh	script to restart shiny server, root privileges needed, we suggest to make this script available for roog
- shiny-server.conf	Shiny server configuration, copy this to /etc/shiny-server/shiny-server.conf
- .bash_profile		Exports necessary for gurobi, copy this to the home folder of shane user

## install packages
```bash 
sudo apt-get update
sudo apt-get install aptitude
sudo aptitude safe-upgrade
sudo aptitude install mc vim screen perl-base r-base wget libssh2-1 libssh2-1-dev gdebi-core libcurl4-openssl-dev libssl-dev libxml2-dev

cd /tmp
wget http://packages.gurobi.com/7.5/gurobi7.5.2_linux64.tar.gz
```

### compatibility
http://www.gurobi.com/products/supported-platforms

## install R
Check compatibility with gurobi:
http://www.gurobi.com/products/supported-platforms
(current version to install 3.4)
```bash 
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb [arch=amd64,i386] https://cran.rstudio.com/bin/linux/ubuntu xenial/'
sudo apt-get update
sudo aptitude install r-base=3.4.3-1xenial0
```

## shiny

follow the instructions at:
https://www.digitalocean.com/community/tutorials/how-to-set-up-shiny-server-on-ubuntu-16-04
```bash
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
```

```bash
wget https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.6.875-amd64.deb
sudo gdebi shiny-server-1.5.6.875-amd64.deb
```
add shiny user
```bash
adduser shane
```

The shiny configuration file is: /etc/shiny-server/shiny-server.conf



## install r markdown
```bash
sudo su - -c "R -e \"install.packages('rmarkdown', repos='http://cran.rstudio.com/')\""
```
## install gurobi
http://www.gurobi.com/documentation/7.5/quickstart_linux.pdf
insert this into shane's .bashrc:
export           GUROBI_HOME="/opt/gurobi752/linux64"
export           PATH="${PATH}:${GUROBI_HOME}/bin"
export           LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"

```bash
tar xvfz gurobi7.5.2_linux64.tar.gz
mv gurobi752 /opt/
su - shane
grbgetkey
```

## install gurobi in R
(as shane)
```R
install.packages('slam')
install.packages('/opt/gurobi752/linux64/R/gurobi_7.5-2_R_x86_64-pc-linux-gnu.tar.gz', repos=NULL)
```
### check gcc version
```bash
gcc -v
```
IMPORTANT:
Use libgurobi_g++5.2.a for C++ in Ubuntu 16.04, 16.10, and 17.04
```bash
ln -s libgurobi_g++5.2.a libgurobi_c++.a 
```
### install rgurobi package
```R
if (!require('devtools'))
        install.packages('devtools', repo='http://cran.rstudio.com', dep=TRUE)
devtools:::install_github('paleo13/rgurobi')
```

## install GRNOpt

### generate ssh key-pair
```bash
mkdir ~/.ssh
chmod 700 ~/.ssh
ssh-keygen -t rsa
```
add public key to LUMS
### install LibSSH2
sudo apt-get install libssh2-1 libssh2-1-dev

### Check ssh
(within R)
```R
#If ssh is not active then libssh has to be installed, then re-install git2r
#sudo apt-get install libssh2-1 libssh2-1-dev
within R
library(git2r)
libgit2_features()
library(git2r)
libgit2_features()
```

```R
cred = git2r::cred_ssh_key(publickey = "/home/shane/.ssh/id_rsa.pub", privatekey = "/home/shane/.ssh/id_rsa")
devtools::install_git("ssh://git@git-r3lab-server.uni.lu:8022/andras.hartmann/Logic_Pipeline.git", subdir="GRNOpt", branch="master", credentials = cred)
```

## seesaw install

### install required packages (bioconductor)
```R
source("https://bioconductor.org/biocLite.R")
biocLite(c("geneplotter","limma","mouse430a2.db","oligo","mogene10sttranscriptcluster.db","mogene11sttranscriptcluster.db"))
#better to install GEOQuery from github
library(devtools)
install_github('GEOquery','seandavi')
```
### install perl module graph::directed
(as root)
```bash
cpan App::cpanminus
cpanm Graph::Directed
```

### Install seesaw module
```R
cred = git2r::cred_ssh_key(publickey = "/home/shane/.ssh/id_rsa.pub", privatekey = "/home/shane/.ssh/id_rsa")
devtools::install_git("ssh://git@git-r3lab-server.uni.lu:8022/andras.hartmann/seesaw.git", branch="master", credentials = cred)
```
### Create symbolic link to the web application
```bash
ln -s /home/shane/R/x86_64-pc-linux-gnu-library/3.4/seesaw/webapp/www/ ./webapp
```

