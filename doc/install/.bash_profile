#Exports needed for gurobi
#Copy this to /home/shane/.bash_profile
export           GUROBI_HOME="/opt/gurobi752/linux64"
export           PATH="${PATH}:${GUROBI_HOME}/bin"
export           LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
